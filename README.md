# Kitty

[![GPLv3 license](https://img.shields.io/gitlab/license/appuchia-dotfiles/kitty?style=flat-square)](https://gitlab.com/appuchia-dotfiles/kitty/-/blob/master/LICENSE)
[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://gitlab.com/appuchia-dotfiles)

## Work in progress. Setup will be available

## License

This project is licensed under the [GPLv3 license](https://gitlab.com/appuchia-dotfiles/kitty/-/blob/master/LICENSE).

Repo icon is [whiskers](https://github.com/samholmes/whiskers), licensed under the MIT License.

Coded with 🖤 by Appu
